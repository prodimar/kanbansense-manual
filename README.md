# Manual Kanbansense

Kanbansense es un equipo cuya funcionalidad principal es detectar la existencia 
de contenido en las cuatro secciones de una cesta Kanban, y transmitir esta 
información a través de señalización física y una interfaz Bluetooth Low Energy.

A lo largo de este documento, se denomina "sensorización" el proceso de detección 
de objetos en la cesta.

## Guía rápida

Para la puesta en marcha del equipo una vez recibido han de seguirse los siguientes 
pasos:

1. Abrir compartimento de baterías y colocar baterías. El equipo indicará su 
activación con sendos parpadeos de sus leds en color ámbar y entrará en estado 
inactivo, a la espera de ser instalado y puesto en marcha.

2. Instalar en el centro de una "cesta Kanban", acoplándolo en su centro. Esta 
primera unidad se suministra ensamblada con los adaptadores de altura más cortos, 
lo que permite instalarlo únicamente en una "cesta Kanban" baja.

3. Comandar la calibración del equipo mediante una pulsación "media" del botón, 
manteniéndolo en torno a 2 segundos pulsado y liberándolo:

  - La calibración / reconocimiento del entorno ha de realizarse con los 
  **compartimentos vacíos**.

  - El proceso de reconocimiento tarda unos 5 segundos en completarse. Durante 
  ese tiempo, los leds de compartimento parpadean en ámbar.

4. Una vez completado el reconocimiento, el equipo realizará observaciones 
periódicas del estado de los compartimentos y emitirá 1 vez por segundo un parpadeo 
en verde en cada led de compartimento que observe vacío.

5. Si el equipo se reinstala en una cesta distinta, debe comandarse su recalibración 
siguiendo los pasos del punto 3.

## Estado del desarrollo

El diseño electrónico y mecánico del equipo ha sido ya completamente validado. 
Respecto al firmware programado en la unidad entregada, el estado actual de su 
desarrollo permite evaluar de forma general la funcionalidad del sistema y además 
es actualizable vía BLE, por lo que en sucesivas entregas de imágenes de firmware 
se completarán los puntos pendientes, listados a continuación:

- Consumo del equipo no optimizado: especialmente debido a la configuración 
programada para facilitar la evaluación del equipo.

- Configuración/calibración no persistente a pérdidas de alimentación 
(retirada de baterías).

- Sensibilidad de detección no optimizada: el equipo puede no detectar objetos 
pequeños a distancias grandes.

- Parámetros de operación no configurables.

La unidad está configurada con los siguientes parámetros de operación, inadecuados 
para la explotación del equipo en una instalación, pero sí especialmente convenientes 
para la evaluación del mismo:

- Periodo de sensorización de compartimentos una vez puesto en marcha: 
5 s. inicio + 5 s. sensado = ~10 s.

- Periodo de envío de baliza BLE: 250 ms.

- Periodo de parpadeo de señalización de compartimento vacío: 1 s.

Con la configuración descrita el tiempo de vida de las baterías es de ~150 horas 
en estado activo (sensorización activa), por lo que se recomienda retirar las 
baterías cuando no se esté evaluando su funcionamiento.

## Interfaz física de usuario

### LEDs

El equipo presenta cinco LEDs; cuatro de ellos ubicados en las esquinas, y asociados 
a los cajones de la cesta Kanban, y uno situado en el centro de la parte frontal, 
denominado LED central o de estado.

Cada uno de los LEDs de cajón parpadea cuando el cajón asociado está vacío. La 
frecuencia y el color de los parpadeos son configurables.

El LED central indica el estado del equipo, que puede encontrarse entre los siguientes:

  - **Configuración de fábrica**: parpadea en color verde cada 10 segundos.
  - **Comportamiento normal**: parpadea en color verde cada 2 segundos.
  - **Comportamiento anormal o error**: parpadea en color rojo cada 2 segundos.
  - **Conexión Bluetooth activa**: parpadea cada segundo. Se mantiene el color 
    mostrado anteriormente, para permitir identificar si existe algún error.
  - **Identificación física**: parpadea cada medio segundo, manteniendo el color 
    mostrado anteriormente. Este estado tiene una duración limitada y sólo se 
    puede activar enviando un comando específico al equipo.

### Botón

El botón, situado en la parte central delantera (justo delante del LED de estado),
diferencia tres tipos de pulsaciones.

  - **Pulsación corta:** el botón está pulsado durante un tiempo menor a 700 
    milisegundos. Provoca una sensorización
  - **Pulsación media:** el botón está pulsado entre 700 y 2500 milisegundos. 
    Provoca una calibración.
  - **Pulsación larga:** el botón está pulsado más de 2500 milisegundos. Provoca 
    un cambio en el contenido de la baliza, de modo que se pueda identificar el 
    equipo entre los dispositivos Bluetooth disponibles.


## Interfaz Bluetooth

### Estructura baliza

La baliza contiene 28 bytes de información. Los 7 primeros bytes son información 
necesaria para la interpretación de la baliza y el establecimiento de conexiones; 
los datos de aplicación comienzan en el byte 8.

  - ***Flags* del dispositivo**: máscara de bits que utiliza el byte 8.

    - Bit 0: es 1 si está activada la identificación del dispositivo por medio de 
      la baliza, y 0 si no. La activación se puede provocar realizando una pulsación 
      larga sobre el botón, y dura un tiempo determinado; permite identificar un 
      equipo concreto entre los dispositivos Bluetooth.

  - **Estado del dispositivo**: secuencia de máscaras de bits que detallan el estado 
    de cada cajón. Comienzan en el byte 9, hay tantas máscaras como cajones y cada
    una utiliza 4 bytes.

    - Bits 0 a 13: estado detallado de los sensores.
    - Bit 14 (estado válido): activo si los sensores han sido inicializados.
    - Bit 15 (calibrado): activo si los sensores han sido calibrados.
    - Bit 16 (error): activo si hubo algún error o comportamiento inesperado.
    - Bit 17 (vacío): activo si no se detectan objetos en el cajón.

El equipo se identifica con el nombre acortado "KS". Es posible que, por razones 
relacionadas con la caché Bluetooth del dispositivo cliente, se observe otro nombre 
en la lista de equipos visibles.

### Comandos

Se pueden provocar acciones sobre un Kanbansense enviándole comandos; el envío 
se realiza escribiendo la característica GATT correspondiente. Todas las características 
se agrupan en el servicio "Kanbansense Commands" (UUID `73245b66-fd3c-4eef-b917-00c9518fa4af`).

| Nombre             | Acción                                                   | Valores        | UUID                                   |
|:-------------------|:---------------------------------------------------------|:---------------|:---------------------------------------|
| Sample             | Causa una sensorización                                  | Cualquier byte | `be0699ee-ed63-4e46-b5b7-305e67e684bf` |
| Calibrate          | Causa una calibración                                    | Cualquier byte | `5c296327-d63a-47ae-b279-32aca6854916` |
| LED Signal Trigger | Activa la identificación física a través del LED central | Cualquier byte | `9df341aa-74e1-4c5d-a5ee-4aff1fe873da` |
| Battery Sample     | Causa una medida de batería                              | Cualquier byte | `d2d81072-cbd4-450d-b536-7fd70e0dc78b` |
| Reset Device       | Reinicia el dispositivo                                  | Cualquier byte | `fe57c936-0396-4287-adcd-e290fd71f9ba` |

### Variables de estado

En construcción.

### Parámetros de configuración

En construcción.

## Actualización de firmware

La actualización de firmware se realiza a través de Bluetooth. Para ello, se 
proporcionarán dos ficheros: `application.gbl` y `apploader.gbl`. Una actualización 
parcial contempla únicamente la escritura de `application.gbl` en el equipo, 
mientras que una actualización total requiere que se escriban ambos ficheros.

Para la actualización es necesario utilizar la aplicación para Android e iOS **EFR 
Connect**, que permite la conexión y comunicación con dispositivos Bluetooth. Una 
vez establecida una conexión con un Kanbansense, es posible comenzar el proceso 
abriendo el menú contextual situado en la esquina superior derecha, y clicando en
la opción "OTA".

![menu_contextual_1](img/menu_contextual_1.JPEG "Menú contextual")
![menu_contextual_2](img/menu_contextual_2.JPEG "Menú contextual: opción OTA")

Se desplegará un diálogo que mostrará y permitirá modificar los ajustes de la 
actualización. Existen dos pestañas, una etiquetada como "Partial OTA" y otra 
etiquetada como "Full OTA"; en la primera sólo se solicita el fichero `application.gbl`,
mientras que en la segunda es necesario cargar también `apploader.gbl`. Se 
recomienda seleccionar siempre la segunda opción para evitar errores.

![partial_ota](img/partial_ota.JPEG "Actualización parcial")
![full_ota](img/full_ota.JPEG "Actualización completa")

No es necesario modificar ningún otro parámetro para lograr una actualización 
con éxito.

Una vez indicados los ficheros de actualización, se habilita el botón etiquetado 
como "OTA" en la parte inferior del diálogo, que permite lanzar el proceso. La
aplicación mostrará una barra de progreso para cada una de las fases (una en 
actualización parcial, y dos en actualización completa).

![ota](img/ota.JPEG "Proceso de actualización")

Una vez terminado el proceso, se habilita el botón "END"; hay que clicarlo para
reiniciar el equipo en modo de funcionamiento normal.

En caso de error, el equipo permanecerá en modo OTA; este fenómeno se puede detectar 
conectándose al equipo y comprobando que no describe todos los servicios especificados. 
Se puede restaurar el funcionamiento normal realizando una actualización con éxito.